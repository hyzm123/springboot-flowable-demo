package com.example.springbootflowabledemo.service;

import com.example.springbootflowabledemo.domian.vo.ResponseBean;
import com.example.springbootflowabledemo.domian.vo.VacationApproveVo;
import com.example.springbootflowabledemo.domian.vo.VacationRequestVo;

public interface VacationService {

	ResponseBean askForLeave(VacationRequestVo vacationRequestVO);
	ResponseBean leaveList(String identity);

	ResponseBean askForLeaveHandler(VacationApproveVo vacationVO);

	ResponseBean searchResult(String name);
}
