package com.example.springbootflowabledemo.service;

import com.example.springbootflowabledemo.domian.resp.MyProcessResp;
import com.example.springbootflowabledemo.domian.resp.PageResp;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface FlowableService {
    /**
     * 部署流程模板
     *
     * @param name 名称
     * @param in   流程模板流
     */
    void deployment(String name, InputStream in);

    /**
     * 启动流程
     *
     * @param procdefId 部署的流程模板ID
     * @param variables 流程需要的参数
     */
    void activate(String procdefId, Map<String, Object> variables);

    /**
     * 完成一个节点流程
     *
     * @param taskId    该节点ID
     * @param variables 参数
     */
    void complete(String taskId, Map<String, Object> variables);

    /**
     * 删除流程定义
     *
     * @param deployId
     */
    void deleteDeploy(String deployId);

    /**
     * 挂起/激活流程定义
     *
     * @param procdefId
     * @param state     1:激活, 2:挂起
     */
    String updateDeployState(String procdefId, Integer state);

    /**
     * 挂起/激活流程实例
     *
     * @param processInstanceId
     * @param state             1:激活, 2:挂起
     * @return
     */
    String updateProcessState(String processInstanceId, Integer state);

    /**
     * 查询我启动的流程
     */
    List<MyProcessResp> myStartProcess();


    /**
     * 查询当前登录用户代办列表
     *
     * @param pageNum
     * @param pageSize
     */
    PageResp myTodoList(Integer pageNum, Integer pageSize);

    /**
     * 一键部署
     * @param processModelId    ACT_DE_MODEL 主键
     * @param processModelHistoryId ACT_DE_MODEL_HISTORY 主键
     */
    void aKeyDeployment(String processModelId, String processModelHistoryId);
}
