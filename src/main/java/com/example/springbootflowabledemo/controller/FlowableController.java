package com.example.springbootflowabledemo.controller;

import com.example.springbootflowabledemo.domian.req.ActivateReq;
import com.example.springbootflowabledemo.domian.req.CompleteReq;
import com.example.springbootflowabledemo.service.FlowableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/flowable-demo")
public class FlowableController {

    @Autowired
    private FlowableService flowableService;

    /**
     * 流程部署
     *
     * @param file
     * @return
     * @throws Exception
     */
    @PostMapping("/deploy")
    public String deploy(MultipartFile file) throws Exception {
        flowableService.deployment(file.getOriginalFilename(), file.getInputStream());
        return "流程部署成功!";
    }

    /**
     * 流程启动
     *
     * @param req
     * @return
     */
    @PostMapping("/activate")
    public String activate(@RequestBody ActivateReq req) {
        flowableService.activate(req.getProcdefId(), req.getVariables());
        return "流程启动成功!";
    }

    /**
     * 节点完成
     *
     * @param req
     * @return
     */
    @PostMapping("/complete")
    public String complete(@RequestBody CompleteReq req) {
        flowableService.complete(req.getTaskId(), req.getVariables());
        return "节点处理完成!";
    }

    /**
     * 删除流程定义相关所有数据
     *
     * @param deployId
     * @return
     */
    @PostMapping("/delete/{deployId}")
    public String deleteDeploy(@PathVariable("deployId") String deployId) {
        flowableService.deleteDeploy(deployId);
        return "流程定义删除成功!";
    }

    /**
     * 激活/挂起流程定义
     *
     * @param procdefId test:1:15bac5c7-1dd3-11ed-a283-3cf8620b8a2a
     * @param state     1:激活, 2:挂起
     * @return
     */
    @PostMapping("/deploy/{procdefId}/{state}")
    public String updateDeployState(@PathVariable("procdefId") String procdefId, @PathVariable("state") Integer state) {
        return flowableService.updateDeployState(procdefId, state);
    }


    /**
     * 激活/挂起流程实例
     *
     * @param processInstanceId e54f94bf-1dd9-11ed-82ff-3cf8620b8a2a
     * @param state             1:激活, 2:挂起
     * @return
     */
    @PostMapping("/process/{processInstanceId}/{state}")
    public String updateProcessState(@PathVariable("processInstanceId") String processInstanceId, @PathVariable("state") Integer state) {
        return flowableService.updateProcessState(processInstanceId, state);
    }

    /**
     * 获取我启动的发起的流程
     */
    @GetMapping("/process/start")
    public Object myStartProcess() {
        return flowableService.myStartProcess();
    }

    /**
     * 查看我的代办
     *
     * @return
     */
    @GetMapping("/process/todoList")
    public Object myTodoListProcess(Integer pageNum, Integer pageSize) {
        return flowableService.myTodoList(pageNum, pageSize);
    }


    @PostMapping("/aKeyDeployment")
    public Object aKeyDeployment(String processModelId, String processModelHistoryId) {
        flowableService.aKeyDeployment(processModelId, processModelHistoryId);
        return "一键部署成功";
    }
}
