package com.example.springbootflowabledemo.domian.vo;

import lombok.Data;

/**
 * 请假条审批
 * @Date
 */
@Data
public class VacationApproveVo {

	private String taskId;

	private Boolean approve;

	private String name;
}



