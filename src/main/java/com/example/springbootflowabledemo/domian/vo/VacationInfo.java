package com.example.springbootflowabledemo.domian.vo;

import java.util.Date;

import lombok.Data;

/**
 * 请假条DO
 * @Date
 */
@Data
public class VacationInfo {

	private String name;

	private Date startTime;

	private Date endTime;

	private String reason;

	private Integer days;

	private Boolean status;
}
