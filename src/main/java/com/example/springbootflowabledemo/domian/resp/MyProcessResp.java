package com.example.springbootflowabledemo.domian.resp;

import lombok.Data;

import java.util.Date;

@Data
public class MyProcessResp {
    /**
     * 流程定义名称
     */
    private String processDefinitionName;
    /**
     * 流程定义ID
     */
    private String processDefinitionId;
    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 流程开始时间
     */
    private Date startDate;
    /**
     * 流程结束时间
     */
    private Date endDate;
}
