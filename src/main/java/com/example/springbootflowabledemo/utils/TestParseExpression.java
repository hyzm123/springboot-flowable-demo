package com.example.springbootflowabledemo.utils;

import org.apache.commons.lang3.StringUtils;

import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

/**
 * flowable 自身对模型的验证也是有局限的，更多的是对排他网关的流设计条件后是否有多条输出流，这样的错误能验证出来，其他的一些类型错误不识别，所以不是万能的。
 *
 * 二、对 UEL 表达式、表达式函数进行分解
 * Flowable 使用 UEL 进行表达式解析。UEL 代表统一表达式语言，是 EE6 规范的一部分。6.4.0版本增加了表达式函数。更多使用说明参考官方文档：https://www.flowable.com/open-source/docs/bpmn/ch04-API/#expression-functions
 * ————————————————
 * 版权声明：本文为CSDN博主「MTonj」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
 * 原文链接：https://blog.csdn.net/MTonj/article/details/124331834
 */
public class TestParseExpression {
	public static void main(String[] args) {
		String expression ="${vars:getOrDefault(user1, '1')},${vars:getOrDefault(user2, '19')},${vars:getOrDefault(user3, '22')}";
		List<String> result=TestParseExpression.parseDelimitedList(expression);
		System.out.println(result);
	}


	public static List<String> parseDelimitedList(String s) {
		List<String> result = new ArrayList<>();
		if (StringUtils.isNotEmpty(s)) {

			StringCharacterIterator iterator = new StringCharacterIterator(s);
			char c = iterator.first();

			StringBuilder strb = new StringBuilder();
			boolean insideExpression = false;

			while (c != StringCharacterIterator.DONE) {
				if (c == '{' || c == '$') {
					insideExpression = true;
				} else if (c == '}') {
					insideExpression = false;
				} else if (c == ',' && !insideExpression) {
					result.add(strb.toString().trim());
					strb.delete(0, strb.length());
				}

				if (c != ',' || insideExpression) {
					strb.append(c);
				}

				c = iterator.next();
			}

			if (strb.length() > 0) {
				result.add(strb.toString().trim());
			}

		}
		return result;
	}
}
