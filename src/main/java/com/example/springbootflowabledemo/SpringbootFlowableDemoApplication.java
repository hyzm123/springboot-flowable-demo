package com.example.springbootflowabledemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFlowableDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFlowableDemoApplication.class, args);
	}

}
