package com.example.springbootflowabledemo.controller;

import lombok.extern.slf4j.Slf4j;
import org.flowable.engine.HistoryService;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.ui.modeler.serviceapi.ModelService;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ask_for_leave.bpmn20.xml
 * https://blog.csdn.net/pengming97/article/details/127027694
 */
@Slf4j
@SpringBootTest
public class TestHello {
	@Resource
	private RuntimeService runtimeService;
	@Resource
	private TaskService taskService;

	@Resource
	private RepositoryService repositoryService;

	@Resource
	private HistoryService historyService;

	@Resource
	protected ModelService modelService;
	String staffId = "12";
	String zuzhangId = "90";
	String managerId = "80";

	/**
	 * 开启一个流程
	 */
	@Test
	public void askForLeave() {

		HashMap<String, Object> map = new HashMap<>();
		map.put("leaveTask", staffId);
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("ask_for_leave", map);
		runtimeService.setVariable(processInstance.getId(), "name", "javaboy");
		runtimeService.setVariable(processInstance.getId(), "reason", "休息一下");
		runtimeService.setVariable(processInstance.getId(), "days", 10);
		log.info("创建请假流程 processId：{}", processInstance.getId());
	}


	/**
	 * 提交给组长审批
	 */
	@Test
	public void submitToZuzhang() {
		//员工查找到自己的任务，然后提交给组长审批
		List<Task> list = taskService.createTaskQuery().taskAssignee(staffId).orderByTaskId().desc().list();
		for (Task task : list) {
			log.info("任务 ID：{}；任务处理人：{}；任务是否挂起：{}", task.getId(), task.getAssignee(), task.isSuspended());
			Map<String, Object> map = new HashMap<>();
			//提交给组长的时候，需要指定组长的 id
			map.put("zuzhangTask", zuzhangId);
			taskService.complete(task.getId(), map);
		}
	}


	/**
	 * 组长审批-批准
	 */
	@Test
	public void zuZhangApprove() {
		List<Task> list = taskService.createTaskQuery().taskAssignee(zuzhangId).orderByTaskId().desc().list();
		for (Task task : list) {
			log.info("组长 {} 在审批 {} 任务", task.getAssignee(), task.getId());
			Map<String, Object> map = new HashMap<>();
			//组长审批的时候，如果是同意，需要指定经理的 id
			map.put("managerTask", managerId);
			map.put("checkResult", "通过");
			taskService.complete(task.getId(), map);
		}
	}

	/**
	 * 组长审批-拒绝
	 */
	@Test
	public void zuZhangReject() {
		List<Task> list = taskService.createTaskQuery().taskAssignee(zuzhangId).orderByTaskId().desc().list();
		for (Task task : list) {
			log.info("组长 {} 在审批 {} 任务", task.getAssignee(), task.getId());
			Map<String, Object> map = new HashMap<>();
			//组长审批的时候，如果是拒绝，就不需要指定经理的 id
			map.put("checkResult", "拒绝");
			taskService.complete(task.getId(), map);
		}
	}

	/**
	 * 经理审批自己的任务-批准
	 */
	@Test
	public void managerApprove() {
		List<Task> list = taskService.createTaskQuery().taskAssignee(managerId).orderByTaskId().desc().list();
		for (Task task : list) {
			log.info("经理 {} 在审批 {} 任务", task.getAssignee(), task.getId());
			Map<String, Object> map = new HashMap<>();
			map.put("checkResult", "通过");
			taskService.complete(task.getId(), map);
		}
	}

	/**
	 * 经理审批自己的任务-拒绝
	 */
	@Test
	public void managerReject() {
		List<Task> list = taskService.createTaskQuery().taskAssignee(managerId).orderByTaskId().desc().list();
		for (Task task : list) {
			log.info("经理 {} 在审批 {} 任务", task.getAssignee(), task.getId());
			Map<String, Object> map = new HashMap<>();
			map.put("checkResult", "拒绝");
			taskService.complete(task.getId(), map);
		}
	}

	public class AskForLeaveFail implements JavaDelegate {
		@Override
		public void execute(DelegateExecution execution) {
			System.out.println("请假失败。。。");
		}
	}
}
